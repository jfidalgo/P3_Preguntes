package com.example.fidalgo.p3fidalgojavier;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Fidalgo on 12/03/2018.
 */

public class UsuariosSQLiteHelper extends SQLiteOpenHelper {


    //Creacio de taules

    String sqlCreateTableUsuarios = "CREATE TABLE Usuarios (nombre TEXT, operacio TEXT, superat BOOLEAN)";


    String sqlCreateTableTema = "CREATE TABLE Tema (nombre TEXT PRIMARY KEY)";


    String sqlCreateTablePregunta = "CREATE TABLE Pregunta (\n" +
            "     idPregunta INTEGER PRIMARY KEY,\n" +
            "     enunciat TEXT NOT NULL,\n" +
            "     nombre TEXT,\n" +
            "     FOREIGN KEY (nombre) REFERENCES tema(nombre)\n" +
            "     );";

    String sqlCreateTableResposta = "CREATE TABLE Resposta (idResposta INTEGER PRIMARY KEY, idPregunta INTEGER ,resposta TEXT, correcta BOOLEAN,\n" +
            "            FOREIGN KEY(idPregunta) REFERENCES Pregunta(idPregunta))";


    //Insert de los temas

    String temaSuma = "INSERT INTO tema VALUES('suma');";

    String temaResta = "INSERT INTO tema VALUES('resta');";

    String temaMultiplicacio = "INSERT INTO tema VALUES('multiplicacio');";

    String temaDivisio = "INSERT INTO tema VALUES('divisio');";


    public UsuariosSQLiteHelper(Context contexto, String nombre,
                                SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(sqlCreateTableUsuarios);
       // db.execSQL(sqlCreateTableTema);
       // db.execSQL(sqlCreateTablePregunta);
        //db.execSQL(sqlCreateTableResposta);

        /*
        //Insert de los temas
        db.execSQL(temaSuma);
        db.execSQL(temaResta);
        db.execSQL(temaMultiplicacio);
        db.execSQL(temaDivisio);

        ContentValues nuevoRegistro = new ContentValues();
        nuevoRegistro.put("nombre", "Suma");
        db.insert("Tema", null, nuevoRegistro);
*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
/*
        db.execSQL("DROP TABLE IF EXISTS Usuarios");
        db.execSQL(sqlCreateTableUsuarios);
        db.execSQL("DROP TABLE IF EXISTS Pregunta");
        db.execSQL(sqlCreateTablePregunta);
        db.execSQL("DROP TABLE IF EXISTS Resposta");
        db.execSQL(sqlCreateTableResposta);
        db.execSQL("DROP TABLE IF EXISTS Tema");
        db.execSQL(sqlCreateTableTema);
*/
       /* db.execSQL(temaSuma);
        db.execSQL(temaResta);
        db.execSQL(temaMultiplicacio);
        db.execSQL(temaDivisio);
*/
    }




}
