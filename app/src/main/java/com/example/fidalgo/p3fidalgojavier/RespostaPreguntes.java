package com.example.fidalgo.p3fidalgojavier;

/**
 * Created by Fidalgo on 11/03/2018.
 */


public class RespostaPreguntes {


    String numeroPreguntes;

    String tipusPreguntes;

    public RespostaPreguntes(String numero, String tipus) {
        this.numeroPreguntes = numero;
        this.tipusPreguntes = tipus;
    }

    public RespostaPreguntes() {

    }


    public String preguntaSuma1() {

        String operacioSuma1 = "Quan es 2 + 2";

        return operacioSuma1;

    }

    public String preguntaSuma2() {

        String operacioSuma2 = "Quan es 25 + 2";

        return operacioSuma2;

    }

    public String preguntaSuma3() {

        String operacioSuma3 = "Quan es 50 + 75";

        return operacioSuma3;

    }

    public String preguntaSuma4() {

        String operacioSuma4 = "Quan es 75 + 100";

        return operacioSuma4;

    }


    public String preguntaResta1() {

        String operacioResta1 = "Quan es 100 - 25";

        return operacioResta1;

    }

    public String preguntaMultiplicacio1() {

        String operacioMultiplicacio1 = "Quan es 100 * 25";

        return operacioMultiplicacio1;

    }

    public String preguntaDivisio1() {

        String operacioDivisio1 = "Quan es 25 / 5";

        return operacioDivisio1;

    }

}
