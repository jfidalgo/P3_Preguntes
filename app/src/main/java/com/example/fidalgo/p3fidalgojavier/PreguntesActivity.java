package com.example.fidalgo.p3fidalgojavier;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static com.example.fidalgo.p3fidalgojavier.R.id.radioPreguntes1;

public class PreguntesActivity extends AppCompatActivity implements View.OnClickListener {

    TextView editPregunta1;
    RadioButton resposta1;
    RadioButton resposta2;
    RadioButton resposta3;
    String operacions;
    int numeroPreguntes;
    String nomPersona;
    boolean encertat = true;
    boolean primeraRespostaEncertada = false;
    boolean segonaRespostaEncertada = false;
    boolean terceraRespostaEncertada = false;
    int pregunta = 1;
    Button puntuacioButton, preguntaSeguentButton;
    Intent i;

    private SQLiteDatabase db;


    UsuariosSQLiteHelper usdbh =
            new UsuariosSQLiteHelper(this, "DBUsuarios", null, 1);


    List<Pregunta> preguntesTotes = new ArrayList<>();

    public void omplePreguntes() {
        //Alternativa 1: mйtodo rawQuery()
        Cursor c = db.rawQuery("SELECT * FROM Pregunta WHERE tema='" + operacions + "'", null);

        // viewRegistres.setText("");

        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya mas registros
            do {

                int id = c.getInt(0);
                String enunciat = c.getString(1);
                String tema = c.getString(2);


                // viewRegistres.append(" " + nom + " - " + operacio + " - " + aprovat + "\n");
            } while (c.moveToNext());
        }
    }

    //Omple el ArrayList de Preguntes amb totes les repostes de cada pregunta amb la pregunta
    public void ompleRespostes() {

        Cursor c = db.rawQuery("SELECT idPregunta, enunciat, nombre FROM pregunta WHERE nombre LIKE '" + operacions + "' LIMIT 3", null);

        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya m�s registros
            do {
                String idPregunta = c.getString(0);

                List<Resposta> respostes3 = new ArrayList<>();

                Cursor c2 = db.rawQuery("SELECT idPregunta,idResposta, resposta, correcta FROM resposta WHERE idPregunta = '" + idPregunta + "'  LIMIT 3", null);

                //bucle per obtenir les respostes de cada pregunta
                if (c2.moveToFirst()) {

                    do {
                        Resposta resp = new Resposta();
                        String idPregun = c2.getString(0);
                        resp.setIdPregunta(Integer.parseInt(idPregun));

                        String idResposta = c2.getString(1);
                        resp.setIdResposta(Integer.parseInt(idResposta));

                        String resposta = c2.getString(2);
                        resp.setResposta(resposta);


                        String correcta = c2.getString(3);
                        resp.setCorrecta(Boolean.parseBoolean(correcta));

                        respostes3.add(resp);

                    } while (c2.moveToNext());

                }
                String enunciat = c.getString(1);
                String nomTema = c.getString(2);

                //Creacio del objecte pregunta on li pasem totes les dades, incloent el arrayList amb les respostes
                Pregunta pregunta = new Pregunta(Integer.parseInt(idPregunta), enunciat, nomTema, respostes3);
                //Omple el arrayList
                preguntesTotes.add(pregunta);


            } while (c.moveToNext());

            c.close();
        }
    }

    //Carrega i mostra la primera pregunta segons el tema
    public void pregunta1() {

        editPregunta1.setText(preguntesTotes.get(0).enunciat);

        resposta1.setText(String.valueOf(preguntesTotes.get(0).getRespostes().get(0).resposta));
        primeraRespostaEncertada = preguntesTotes.get(0).getRespostes().get(0).correcta;

        resposta2.setText(String.valueOf(preguntesTotes.get(0).getRespostes().get(1).resposta));
        segonaRespostaEncertada = preguntesTotes.get(0).getRespostes().get(1).correcta;

        resposta3.setText(String.valueOf(preguntesTotes.get(0).getRespostes().get(2).resposta));
        terceraRespostaEncertada = preguntesTotes.get(0).getRespostes().get(2).correcta;


    }

    //Carrega i mostra la segona pregunta segons el tema
    public void pregunta2() {
        editPregunta1.setText(preguntesTotes.get(1).enunciat);

        resposta1.setText(String.valueOf(preguntesTotes.get(1).getRespostes().get(0).resposta));
        primeraRespostaEncertada = preguntesTotes.get(1).getRespostes().get(0).correcta;

        resposta2.setText(String.valueOf(preguntesTotes.get(1).getRespostes().get(1).resposta));
        segonaRespostaEncertada = preguntesTotes.get(1).getRespostes().get(1).correcta;

        resposta3.setText(String.valueOf(preguntesTotes.get(1).getRespostes().get(2).resposta));
        terceraRespostaEncertada = preguntesTotes.get(1).getRespostes().get(2).correcta;


    }

    //Carrega i mostra la tercera pregunta segons el tema
    public void pregunta3() {
        editPregunta1.setText(preguntesTotes.get(2).enunciat);

        resposta1.setText(String.valueOf(preguntesTotes.get(2).getRespostes().get(0).resposta));
        primeraRespostaEncertada = preguntesTotes.get(2).getRespostes().get(0).correcta;

        resposta2.setText(String.valueOf(preguntesTotes.get(2).getRespostes().get(1).resposta));
        segonaRespostaEncertada = preguntesTotes.get(2).getRespostes().get(1).correcta;

        resposta3.setText(String.valueOf(preguntesTotes.get(2).getRespostes().get(2).resposta));
        terceraRespostaEncertada = preguntesTotes.get(2).getRespostes().get(2).correcta;


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preguntes);

        db = usdbh.getWritableDatabase();


        i = new Intent(this, PuntuacioActivity.class);

        //Controlador del TextView on es mostrara la pregunta
        editPregunta1 = (TextView) findViewById(R.id.textViewPregunta1);


        //Controlador del radioButton on els mostrara la primera resposta
        resposta1 = (RadioButton) findViewById(radioPreguntes1);

        //Controlador del radioButton on es mostrara la segona resposta
        resposta2 = (RadioButton) findViewById(R.id.radioPreguntes2);

        //Controlador del radioButton on es mostrara la tercera resposta
        resposta3 = (RadioButton) findViewById(R.id.radioPreguntes3);

        //Controlador del button que ens cambiara a la ultima activity
        puntuacioButton = (Button) findViewById(R.id.buttonPuntuacio);
        puntuacioButton.setOnClickListener(this);

        //Controlador del button que canviara de pregunta i respotes
        preguntaSeguentButton = (Button) findViewById(R.id.button);
        preguntaSeguentButton.setOnClickListener(this);


        //Paquet de intents
        Bundle extras = getIntent().getExtras();
        //Data de naixement
        operacions = extras.getString("operacio");
        //Nom de la persona
        numeroPreguntes = extras.getInt("numeroPreguntes");

        //Lloc de naixement
        nomPersona = extras.getString("nomPersona");

        //------------------------------------------------------------------------------------------

        //Si nomes hi ha una pregunta amaguem el boto per canviar de pregunta
        if (numeroPreguntes == 1) {
            preguntaSeguentButton.setVisibility(View.GONE);
            //Si hi ha 2 o 3 preguntes amaguem el boto que ens permet passar a la ultima activity
        } else if (numeroPreguntes == 2 || numeroPreguntes == 3) {
            puntuacioButton.setVisibility(View.GONE);
        }

        //------------------------------------------------------------------------------------------

        //Metode que omple el array amb totes les preguntes amb les seves respostes
        ompleRespostes();

        //------------------------------------------------------------------------------------------
        //Metode per carrega la primera pregunta amb les respostes
        pregunta1();

        //------------------------------------------------------------------------------------------

    }

    //Metode per bloquejar els radioButtons
    public void bloquejar() {
        resposta1.setEnabled(false);
        resposta2.setEnabled(false);
        resposta3.setEnabled(false);
    }
    //Metode per desbloquejar els radioaButtons
    public void desbloquejar() {
        resposta1.setEnabled(true);
        resposta2.setEnabled(true);
        resposta3.setEnabled(true);
    }


    /**
     * Metode per comprobar en quin radioButton hem fet click i si la resposta es correcta
     * @param view
     */
    public void onRadioButtonClicked(View view) {


        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case radioPreguntes1:
                if (checked) {
                    if (primeraRespostaEncertada) {
                        Toast toast2 = Toast.makeText(getApplicationContext(),
                                "Resposta encertada", Toast.LENGTH_SHORT);

                        toast2.show();

                        bloquejar();
                    } else {
                        Toast toast2 = Toast.makeText(getApplicationContext(),
                                "Resposta incorrecte", Toast.LENGTH_SHORT);

                        toast2.show();

                        //Canvi d'estat al no encertar
                        encertat = false;
                    }

                    //Bloqueja el radioButton una vegada ja hem fet click
                    bloquejar();
                }

                break;
            case R.id.radioPreguntes2:
                if (checked)

                    if (segonaRespostaEncertada) {
                        Toast toast2 = Toast.makeText(getApplicationContext(),
                                "Resposta encertada", Toast.LENGTH_SHORT);

                        toast2.show();


                    } else {
                        Toast toast2 = Toast.makeText(getApplicationContext(),
                                "Resposta incorrecte", Toast.LENGTH_SHORT);

                        toast2.show();

                        //Canvi d'estat al no encertar
                        encertat = false;
                    }
                //Bloqueja el radioButton una vegada ja hem fet click
                bloquejar();
                break;
            case R.id.radioPreguntes3:
                if (checked)
                    if (terceraRespostaEncertada) {
                        Toast toast2 = Toast.makeText(getApplicationContext(),
                                "Resposta encertada", Toast.LENGTH_SHORT);

                        toast2.show();


                    } else {
                        Toast toast2 = Toast.makeText(getApplicationContext(),
                                "Resposta incorrecte", Toast.LENGTH_SHORT);

                        toast2.show();

                        //Canvi d'estat al no encertar
                        encertat = false;
                    }
                //Bloqueja el radioButton una vegada ja hem fet click
                bloquejar();
                break;

        }
    }

    @Override
    public void onClick(View v) {


        //Boto per passar a la ultima activity
        if (v.getId() == R.id.buttonPuntuacio) {
            //Creacio del intent
            i.putExtra("encertat", encertat);
            i.putExtra("operacio", operacions);
            i.putExtra("nomPersona", nomPersona);

            startActivity(i);

        } else if (v.getId() == R.id.button) {
            //Cada vegada que fem click al boto per canviar de pregunta incrementa
            //la variable pregunta per saber en quina pregunta ens trobem
            ++pregunta;

            //si ens trobem en la segona pregunta on nomes hi han dos preguntes, es adir, a la ultima,
            //actualitzem la pregunta i les respostes, mostrem el boto per finalitzar i
            //amaguem el boto per canviar de pregunta
            if (pregunta == 2 && numeroPreguntes == 2) {
                pregunta2();
                desbloquejar();
                preguntaSeguentButton.setVisibility(View.GONE);
                puntuacioButton.setVisibility(View.VISIBLE);
                //Si ens trobem a la segona pregunta on hi ha 3, actualitzem a la segona pregunta
            } else if (pregunta == 2 && numeroPreguntes == 3) {
                pregunta2();
                desbloquejar();
                //Si ens trobem a la tercera pregunta on hi ha 3 pregunteas, es a dir, a la ultima,
                //actualitzem a la tercera pregunta i desbloquejem el boto
                //per passar a la ultima activity, amaguem el boto per canviar de pregunta
            } else if (pregunta == 3 && numeroPreguntes == 3) {
                pregunta3();
                desbloquejar();
                preguntaSeguentButton.setVisibility(View.GONE);
                puntuacioButton.setVisibility(View.VISIBLE);
            }

        }
    }
}
